/*
 * CS475/CS675 - Computer Graphics
 *  ToyLOGO Assignment Base Code
 *
 * Copyright 2011, Parag Chaudhuri, Department of CSE, IIT Bombay
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include "render_drawing.hpp"
#include <iostream>
 using namespace std;

void triline(turtle_t &turt, double length){
   if (length <= 0.01){
      turt.forward(length);
   }
   else{
     double newlength = length/3.0;
     triline(turt, newlength);
     turt.turn_left(60);
     triline(turt, newlength);
     turt.turn_right(120);
     triline(turt, newlength);
     turt.turn_left(60);
     triline(turt, newlength);
   }
}
/*
void triline1(turtle_t &turt, double length){
   if (length <= 0.5){
      turt.forward(length);
   }
   else{
     double newlength = length/6.0;
     triline1(turt, 4*newlength);
     turt.turn_left(90);
     triline1(turt, newlength);
     turt.turn_right(90);
     triline1(turt, newlength);
     turt.turn_right(90);
     triline1(turt, newlength);
     turt.turn_left(90);
     triline1(turt, newlength);
     turt.turn_right(90);
     triline1(turt, newlength);
     turt.turn_right(90);
     triline1(turt, newlength);
     turt.turn_left(90);
     triline1(turt, newlength);
     turt.turn_right(90);
     triline1(turt, newlength);
     turt.turn_right(90);
     triline1(turt, newlength);
     turt.turn_left(90);
     triline1(turt, 4*newlength);

   }
}
*/
void triline2(turtle_t &turt, double length){
   if (length <= 0.01){
      turt.forward(length);
   }
   else{
     double newlength = length/3.0;
     triline2(turt, newlength);
     turt.turn_right(90);
     triline2(turt, newlength);
     turt.turn_left(90);
     triline2(turt, newlength);
     turt.turn_left(90);
     triline2(turt, newlength);
     turt.turn_right(90);
     triline2(turt, newlength);
   }
}
//Drawing a Koch Snowflake
void koch(turtle_t &turt, double x){
  turt.reset();
  turt.clear();
  turt.set_pos(0.0, 0.0);
  triline(turt, x);
}
/*
void koch1(turtle_t &turt, double x){
   turt.reset();
   turt.clear();
   turt.set_pos(0.0, 0.0);

   for (int i = 0; i<1; i++)
     {
       triline1(turt, x);
       turt.turn_left(90);
     }
}
*/
//Drawing a Vicsek Fractal
void koch2(turtle_t &turt, double x){
   turt.reset();
   turt.clear();
   turt.set_pos(-0.5, 0.5);

   for (int i = 0; i<4; i++)
     {
       triline2(turt, x);
       turt.turn_right(90);
     }
}

void render_drawing(turtle_t &turt){
  //koch(turt, 1.0);
  koch2(turt, 1.0);
}
